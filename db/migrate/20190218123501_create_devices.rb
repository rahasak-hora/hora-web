class CreateDevices < ActiveRecord::Migration[5.2]
  def change
    create_table :devices do |t|
      t.string :device_id
      t.string :source_ip
      t.string :source_port
      t.string :destination_ip
      t.string :destination_port
      t.string :protocol
      t.references :user, foreign_key: true

      t.timestamps
    end
  end
end

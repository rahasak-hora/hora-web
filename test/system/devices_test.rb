require "application_system_test_case"

class DevicesTest < ApplicationSystemTestCase
  setup do
    @device = devices(:one)
  end

  test "visiting the index" do
    visit devices_url
    assert_selector "h1", text: "Devices"
  end

  test "creating a Device" do
    visit devices_url
    click_on "New Device"

    fill_in "Destination ip", with: @device.destination_ip
    fill_in "Destination port", with: @device.destination_port
    fill_in "Device", with: @device.device_id
    fill_in "Protocol", with: @device.protocol
    fill_in "Source ip", with: @device.source_ip
    fill_in "Source port", with: @device.source_port
    fill_in "User", with: @device.user_id
    click_on "Create Device"

    assert_text "Device was successfully created"
    click_on "Back"
  end

  test "updating a Device" do
    visit devices_url
    click_on "Edit", match: :first

    fill_in "Destination ip", with: @device.destination_ip
    fill_in "Destination port", with: @device.destination_port
    fill_in "Device", with: @device.device_id
    fill_in "Protocol", with: @device.protocol
    fill_in "Source ip", with: @device.source_ip
    fill_in "Source port", with: @device.source_port
    fill_in "User", with: @device.user_id
    click_on "Update Device"

    assert_text "Device was successfully updated"
    click_on "Back"
  end

  test "destroying a Device" do
    visit devices_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Device was successfully destroyed"
  end
end

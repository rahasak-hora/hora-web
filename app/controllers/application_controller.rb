class ApplicationController < ActionController::Base
  protect_from_forgery

  layout :user_layout

  private

    def user_layout
      if current_user
        'application_with_side_bar'
      else
        'application'
      end
    end
end

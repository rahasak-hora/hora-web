# README

* Ruby: 2.6.0

* Rails 5.2.2

* Mysql 5.7

## DockerFile

```
FROM ruby:2.6.0

RUN apt-get update -qq && apt-get install -y build-essential libpq-dev nodejs

RUN mkdir /octopus_app
WORKDIR /octopus_app

ADD Gemfile /octopus_app/Gemfile
ADD Gemfile.lock /octopus_app/Gemfile.lock

RUN bundle install

Add . /octopus_app
```

## docker-compose

```
version: '2'
services:
  db:
    image: mysql:5.7
    container_name: mysql
    restart: always
    environment:
      MYSQL_ROOT_PASSWORD: ENV[]
      MYSQL_DATABASE: ENV[]
      MYSQL_USER: ENV[]
      MYSQL_PASSWORD: ENV[]
    ports:
      - "3307:3306"
  app:
    container_name: octopus_rails
    build: .
    command: bundle exec rails s -p 3000 -b '0.0.0.0'
    volumes:
      - ".:/hora-web"
    ports:
      - "3001:3000"
    depends_on:
      - db
    links:
      - db
    environment:
      DB_NAME: ENV[]
      DB_USER: ENV[]
      DB_PASSWORD: ENV[]
      DB_HOST: db
```

## Execute

```
docker-compose build
```

```
docker-compose up
```

## Bash to the rails

```
docker exec -it [container] bash
```

### Rails console

```
rails c
```

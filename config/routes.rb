Rails.application.routes.draw do
  devise_for :users
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html

  unauthenticated do
    devise_scope :user do
      root to: "pages#index"
    end
  end

  authenticated do
    # root :to => 'devices#index'
  end

  resources :pages, only: :index
end
